(ns euler.largest-palindrome-product)

(defn find-largest-palindrome-product
  []
  (letfn [(pairs [] (for [x (range 100 999) y (range 100 999)] (* x y)))
          (palindrome? [val] (let [val-seq (seq (str val))] (= val-seq (reverse val-seq))))]
    (apply max (filter palindrome? (pairs)))))