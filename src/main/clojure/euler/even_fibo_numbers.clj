(ns euler.even-fibo-numbers)

(defn even-fibo
  []
  (letfn [(generate-fibo [] (map first (iterate (fn [[a b]] [b (+ a b)]) [1 1])))]
    (reduce + (filter even? (take-while #(< % 4000000) (generate-fibo))))))