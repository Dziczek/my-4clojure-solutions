(ns euler.largest-prime-factor)

(defn find-largest-prime-factor
  [num]
  (letfn [(prime? [val] (not-any? zero? (map #(rem val %) (range 2 (inc (Math/sqrt val))))))
          (find-primes [max-val] (take-while #(< % max-val) (filter prime? (iterate inc 2))))
          (find-prime-factors-of [num] (filter #(zero? (rem num %)) (find-primes (Math/sqrt num))))]
    (apply max (find-prime-factors-of num))))